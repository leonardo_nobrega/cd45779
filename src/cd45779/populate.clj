;; on a plataea repl:
;; (load-file "/Users/leonardo.nobrega/Documents/code/cd45779/src/cd45779/populate.clj")
;; (cd45779/create-and-populate 10000)

(ns cd45779
  (:require [cenx.plataea.cassandra :as c]
            [cenx.plataea.cassandra.init :as init]
            [cenx.plataea.cassandra.rolling-trackers :as track]
            [cenx.plataea.cassandra.session :as session]
            [cenx.plataea.cassandra.util :as cs-util]
            [clojure.test.check.generators :as g]
            [qbits.alia :as alia]
            [qbits.hayt :as hayt]
            [taoensso.timbre :as log]))

(defn insert-test-data
  [test-data]
  ;; silence logs about no configuration loaded
  (log/with-config {:ns-blacklist ["cenx.plataea.config.*"]}
    (as-> test-data $
      (hayt/values $)
      (hayt/insert (keyword (str (c/analytics) "." (:day track/table-names))) $)
      (alia/execute (session/session) $)
      (try $ (catch Exception e (println e))))))

(defn generate-values
  [num-values gen]
  (-> gen
      (g/vector num-values num-values)
      g/generate))

(defn generate-row
  [id metric value]
  {:id id
   :metric metric
   :functional_data {"minimum" value
                     "maximum" value
                     "average" value}})

(defn generate-test-data
  [num-values]
  (let [ids (map #(str "host-" %) (range num-values))
        metrics ["cpu" "mem" "dsk"]
        value-params {:NaN? false :min 1.0 :max 100.0}]
    (map generate-row
         (map #(str "host-" %) (range num-values))
         (map #(-> % (rem 3) metrics) (range num-values))
         (generate-values num-values (g/double* value-params)))))

(defn create-table
  []
  (doto (session/session)
    (init/init-keyspaces)
    (track/create-tables)))

(defn populate
  [num-values]
  (->> num-values
       generate-test-data
       (map insert-test-data)
       dorun))

(defn create-table-and-populate
  [num-values]
  (create-table)
  (populate num-values))

(defn data-keyspace
  [replication-factor]
  (#'init/create-keyspace (c/analytics)
                          {:replication-factor replication-factor
                           :replication-class "SimpleStrategy"}))

(defn create-table-with-replication-factor
  [replication-factor]
  (with-redefs [cenx.plataea.cassandra.init/data-keyspace
                (fn [] (data-keyspace replication-factor))]
    (create-table)))

(defn create-tracker-table
  "Based on rolling-trackers/create-tables"
  [session table-name columns clustering-keys]
  (let [table-prefix (clojure.string/replace table-name
                                             track/table-postfix
                                             "")
        ttl (get track/default-rolling-trackers-ttl-seconds table-prefix)]
    (c/sync-table! session
                   (c/create-table-statement (c/analytics)
                                             table-name
                                             columns
                                             clustering-keys
                                             [:id :metric])
                   columns
                   (cs-util/table-properties (c/analytics)
                                             table-name
                                             :default_time_to_live
                                             ttl))))

(defn drop-and-create
  []
  (let [table-name (:day track/table-names)]
    (dorun (map #(alia/execute (session/session) %)
                [(hayt/use-keyspace (keyword (c/analytics)))
                 (hayt/drop-table (keyword table-name) (hayt/if-exists))]))
    (create-tracker-table (session/session)
                          table-name
                          @#'track/daily-monthly-columns [])))

(defn truncate
  []
  (track/clear-table (session/session) :day))
