# cd45779

A quick experiment with cassandra to see what happens when we drop-and-create a table  with offline nodes.

https://bitbucket.org/cenx-cf/plataea/pull-requests/899/cd-45779-join-tracker-and-short-term/diff#comment-51315816

After the create, the online and offline nodes will have different schema-ids. The schema may be the same, but cassandra will refuse to work with the offline nodes.

~~~~
ERROR 18:40:05 Configuration exception merging remote schema
org.apache.cassandra.exceptions.ConfigurationException: Column family ID mismatch
(found d0baf010-dab4-11e7-8791-2594ca8899eb; expected e2e59c40-daaf-11e7-8791-2594ca8899eb)
~~~~
